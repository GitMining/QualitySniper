package iSESniper.scanner;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.scanner.impl.FolderScanner4Java;

public class TestFolderScanner4Java {
	
	private FolderScanner4Java fsj = new FolderScanner4Java();
	
	@Test
	public void testScan() {
		ArrayList<String> files = new ArrayList<String>();
		try {
			files = fsj.scan("");
			System.out.println(files.size());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetFiles() {
		ArrayList<String> files;
		try {
			files = fsj.scan("D:\\work\\GitMining\\Repos\\TechTest\\JavaParserAtmpt");
			System.out.println(files.size());
			ArrayList<String> jf = fsj.getFiles(files);
			System.out.println(jf.size());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FilesListIsEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
