package iSESniper.parser;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.parser.BlkLogicNodeCounter;
import iSESniper.code.parser.JavaASTMethodTool;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.parser.TestBlkLogicNodeCounter.java
 *  
 *  2018年2月11日	下午5:50:01  
*/

public class TestBlkLogicNodeCounter {

	@Test
	public void testGetSubBlocks() {
		BlkLogicNodeCounter blnc = new BlkLogicNodeCounter();
		try {
			JavaASTTransfer.java2AST("D:\\workspace\\Eclipse\\JavaParserAtmpt"
					+ "\\src\\main\\java\\JavaParserAtmpt\\TestFile.java");
			ArrayList<MethodDeclaration> ms = JavaASTTransfer.getMethodList();
			int n = 0;
			for(MethodDeclaration m: ms) {
				n++;
				BlockStmt bs = JavaASTMethodTool.getMethodBlock(m);
				List<LogicNode> lNodes = blnc.getSubBlocks(bs);
				System.out.println("The logic detail of Method " + Integer.toString(n) + "as below:");
				System.out.println(lNodes.toString());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
