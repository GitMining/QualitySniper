package iSESniper.analyzer.logic;

import java.util.Map;

import org.junit.Test;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.logic.impl.MethodsCCInJava;

/**
 * @author : Magister
 * @fileName : iSESniper.analyzer.logic.TestJavaMethodsCC.java
 * 
 *           2018年2月11日 下午10:05:19
 */

public class TestJavaMethodsCC {

	String propth = "D:\\work\\GitMining\\Repos\\samples\\RepoSpringMVC"
			+ "\\src\\main\\java\\com\\springmvc\\dao\\RepoComparedResultDao.java";

	MethodsCCInJava jfc = new MethodsCCInJava();
	
	@Test
	public void testJavaCC() {
		Map<MethodDeclaration, Integer> mscc = jfc
				.getLogicStatistic("D:\\work\\GitMining\\Repos\\TechTest\\JavaParserAtmpt"
						+ "\\src\\main\\java\\JavaParserAtmpt\\TestFile.java");
		for (MethodDeclaration m : mscc.keySet()) {
			System.out.println(m.getName().toString() + " Cyclomatic Complex is: " + mscc.get(m));
		}

	}
	
	@Test
	public void testJStringCC() {
		Map<String, Integer> mscc = jfc.getMethodsCC(propth);
		System.out.println(mscc);
	}
}
