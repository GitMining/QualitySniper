package iSESniper.analyzer.logic;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.logic.LogicStatistic;
import iSESniper.code.analyzer.logic.impl.MethodLogicDepth;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.analyzer.logic.TestMethodLD.java
 *  
 *  2018年2月11日	下午7:28:05  
*/

public class TestMethodLD {

	@Test
	public void testGetMethodLD() {
		LogicStatistic ldc = new MethodLogicDepth();
		try {
			JavaASTTransfer.java2AST("D:\\work\\GitMining\\Repos\\TechTest\\JavaParserAtmpt"
					+ "\\src\\main\\java\\JavaParserAtmpt\\TestFile.java");
			ArrayList<MethodDeclaration> ms = JavaASTTransfer.getMethodList();
			int n = 0;
			for(MethodDeclaration m: ms) {
				n++;
				int ld = ldc.getLogicStatistic(m);
				System.out.println("The LD of Method " + Integer.toString(n) + " is: " + ld);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
