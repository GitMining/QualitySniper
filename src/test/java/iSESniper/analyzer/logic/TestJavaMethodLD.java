package iSESniper.analyzer.logic;

import java.util.Map;

import org.junit.Test;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.logic.impl.MethodsLDInJava;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.analyzer.logic.TestJavaMethodLD.java
 *  
 *  2018年2月11日	下午10:33:18  
*/

public class TestJavaMethodLD {

	@Test
	public void testJavaLD() {
		MethodsLDInJava mldj = new MethodsLDInJava();
		Map<MethodDeclaration, Integer> msld = mldj.getLogicStatistic("D:\\work\\GitMining\\Repos\\TechTest\\JavaParserAtmpt"
					+ "\\src\\main\\java\\JavaParserAtmpt\\TestFile.java");
		for(MethodDeclaration m: msld.keySet()) {
			System.out.println(m.getName().toString() + " Logic Depth is: " + msld.get(m));
		}
	}
}
