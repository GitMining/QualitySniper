package iSESniper.analyzer.loc;

import org.junit.Test;

import iSESniper.code.analyzer.loc.LinesOfCode;
import iSESniper.code.analyzer.loc.impl.FileLOC;
import iSESniper.code.entity.loc.FileLinesOfCodeEntity;

public class TestFileLOC {
	public LinesOfCode<FileLinesOfCodeEntity> floc = new FileLOC();
	
	@Test
	public void testGetLOC() {
		FileLinesOfCodeEntity loc = floc.getLOC("D:\\work\\GitMining\\Repos\\TechTest\\JavaParserAtmpt\\src\\main\\java\\JavaParserAtmpt\\MyJavaParser.java");
		System.out.println("Target path is: " + loc.getPth());
		System.out.println("Total lines of certain file is: " + Long.toString(loc.getLoc()));
		System.out.println("Blank lines of certain file is: " + Long.toString(loc.getBlkLoc()));
		System.out.print("The each blank line\'s number is: ");
		for(Long l: loc.getBlankNum()) {
			System.out.print(Long.toString(l) + " ");
		}
		System.out.println();
		System.out.print(loc.toString());
	}
}
