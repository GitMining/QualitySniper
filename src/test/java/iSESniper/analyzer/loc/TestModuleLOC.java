package iSESniper.analyzer.loc;

import org.junit.Test;

import iSESniper.code.analyzer.loc.LinesOfCode;
import iSESniper.code.analyzer.loc.impl.ProjectLOC;
import iSESniper.code.entity.loc.ModuleLinesOfCodeEntity;

public class TestModuleLOC{
	private LinesOfCode<ModuleLinesOfCodeEntity> ploc = new ProjectLOC();
	
	@Test
	public void testGetModuleLOC() {
		String location = "D:\\work\\GitMining\\Repos\\TechTest\\JavaParserAtmpt";
		ModuleLinesOfCodeEntity mloce = ploc.getLOC(location);
		System.out.println(mloce.toString());
	}
}