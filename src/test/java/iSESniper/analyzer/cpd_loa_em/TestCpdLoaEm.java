package iSESniper.analyzer.cpd_loa_em;

import java.util.List;

import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;
import org.junit.Test;

import iSESniper.code.analyzer.cpd.DuplicatedCodeDetector;
import iSESniper.code.analyzer.cpd.impl.DuplicatedCodeDetectorImpl;
import iSESniper.code.analyzer.em.EmptyMethod;
import iSESniper.code.analyzer.em.impl.EmptyMethodImpl;
import iSESniper.code.analyzer.loa.LOAInFolder;
import iSESniper.code.analyzer.loa.impl.LOAInFolderImpl;
import iSESniper.code.entity.em.EmptyMethodEntity;
import iSESniper.code.entity.loa.LineOfAnnotateInFilesEntity;
import iSESniper.code.entity.cpd.CpdEntity;

public class TestCpdLoaEm {

	@Test
	public void testCpdLoaEm() {
		String prjtPth = "D:/work/GitMining/Repos/samples/161250040_KylinSECIII/Tagging_Phase_I";
		String myProjectPath = prjtPth.replaceAll("/","\\\\");

		EmptyMethod emptyMethod = new EmptyMethodImpl();
		List<EmptyMethodEntity> emptyMethodEntities = emptyMethod.findEmptyMethod(prjtPth);
		System.out.println(emptyMethodEntities);

		LOAInFolder loaInFolder = new LOAInFolderImpl();
		LineOfAnnotateInFilesEntity annotateInFilesEntity = loaInFolder.countLineInFolder(prjtPth);
		for (LineOfAnnotateInSingleFileEntity lineOfAnnotateInSingleFileEntity:annotateInFilesEntity.getLinesOfAnnotate()
			 ) {
			System.out.println(lineOfAnnotateInSingleFileEntity);
		}

		DuplicatedCodeDetector codeDetector = new DuplicatedCodeDetectorImpl();
		List<CpdEntity> cpdEntities = codeDetector.findDuplicatedCodeList(myProjectPath);
		System.out.println(cpdEntities);
	}
}
