package iSESniper.analyzer.variable;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.vab.impl.MethodParams;
import iSESniper.code.entity.vab.MethodParameters;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 * @author : Magister
 * @fileName : iSESniper.analyzer.variable.TestMethodParams.java
 * 
 *           2018年2月7日 下午6:24:52
 */

public class TestMethodParams {
	@Test
	public void testGetMethodParamsCount() {
		MethodParams mParams = new MethodParams();
		try {
			JavaASTTransfer.java2AST("D:\\work\\GitMining\\Repos\\QualitySniper\\src\\main\\java"
					+ "\\iSESniper\\code\\parser\\JavaASTTransfer.java");
			ArrayList<MethodDeclaration> ms = JavaASTTransfer.getMethodList();
			for (MethodDeclaration m : ms) {
				MethodParameters mp = mParams.getVariableCount(m);
				System.out.print(mp.toString());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
