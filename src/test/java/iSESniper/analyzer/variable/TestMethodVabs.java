package iSESniper.analyzer.variable;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.vab.impl.MethodVabs;
import iSESniper.code.entity.vab.MethodVabDeclarations;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.analyzer.variable.TestMethodVabs.java
 *  
 *  2018年2月8日	下午9:39:17  
*/

public class TestMethodVabs {

	@Test
	public void testGetVabCount() {
		try {
			MethodVabs mv = new MethodVabs();
			JavaASTTransfer.java2AST("D:\\work\\GitMining\\Repos\\QualitySniper\\src\\main\\java"
					+ "\\iSESniper\\code\\parser\\JavaASTTransfer.java");
			ArrayList<MethodDeclaration> ms = JavaASTTransfer.getMethodList();
			int i=0;
			for(MethodDeclaration m: ms) {
				System.out.println(++i);
				MethodVabDeclarations md = mv.getVariableCount(m);
				System.out.println(md);
				System.out.println("Variable Declaration Count: " + Integer.toString(md.getDecs().size()));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
