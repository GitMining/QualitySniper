package iSESniper.analyzer.variable;

import org.junit.Test;

import iSESniper.code.analyzer.vab.impl.JavaFileVabDecs;
import iSESniper.code.entity.vab.MethodVabsInFile;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.analyzer.variable.TestJavaFileVabDec.java
 *  
 *  2018年2月8日	下午11:50:02  
*/

public class TestJavaFileVabDec {
	@Test
	public void testGetAllMethodVabDec() {
		JavaFileVabDecs jfvd = new JavaFileVabDecs();
		MethodVabsInFile mvf = jfvd.getVabStatistic("D:\\work\\GitMining\\Repos\\QualitySniper\\src\\main\\java"
				+ "\\iSESniper\\code\\parser\\JavaASTTransfer.java");
		System.out.println(mvf.toString());
	}
}
