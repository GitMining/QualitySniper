package iSESniper.analyzer.loa;

import org.junit.Test;

import iSESniper.code.analyzer.loa.LOAInFolder;
import iSESniper.code.analyzer.loa.impl.LOAInFolderImpl;
import iSESniper.code.entity.loa.LineOfAnnotateInFilesEntity;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

public class TestLOA {

		@Test
		public void testcpd() {
			String testPath = "D:\\workspace\\Eclipse\\ReviewLabel";
			LOAInFolder loaInFolder = new LOAInFolderImpl();
			LineOfAnnotateInFilesEntity annotateInFilesEntity = loaInFolder.countLineInFolder(testPath);
			System.out.println(annotateInFilesEntity.toString());
			System.out.println("==============================");
			for (LineOfAnnotateInSingleFileEntity annotateInSingleFileEntity : annotateInFilesEntity.getLinesOfAnnotate()) {
				System.out.println(annotateInSingleFileEntity.toString());
			}
		}

	}
