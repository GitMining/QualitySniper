package iSESniper.code.analyzer.em;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.entity.em.EmptyMethodEntity;

public interface FindEMOnMethodLevel {
	public EmptyMethodEntity findEmptyMethod(MethodDeclaration methodDeclaration);
}
