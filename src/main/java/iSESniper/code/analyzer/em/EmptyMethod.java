package iSESniper.code.analyzer.em;

import java.util.List;

import iSESniper.code.entity.em.EmptyMethodEntity;

public interface EmptyMethod {
	public List<EmptyMethodEntity>findEmptyMethod(String folderPath);
}
