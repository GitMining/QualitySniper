package iSESniper.code.analyzer.em.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.em.FindEMOnClassLevel;
import iSESniper.code.analyzer.em.FindEMOnMethodLevel;
import iSESniper.code.entity.em.EmptyMethodEntity;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

public class FindEMOnClassLevelImpl implements FindEMOnClassLevel {

	public List<EmptyMethodEntity> findEmptyMethods(File file) {
		FindEMOnMethodLevel methodLevel = new FindEMOnMethodLevelImpl();
		List<EmptyMethodEntity> emptyMethodEntities = new ArrayList<EmptyMethodEntity>();
		try {
			JavaASTTransfer.java2AST(file.getAbsolutePath());
			ArrayList<MethodDeclaration> methods = JavaASTTransfer.getMethodList();
			for (MethodDeclaration methodDeclaration : methods) {
				EmptyMethodEntity emptyMethodEntity = methodLevel.findEmptyMethod(methodDeclaration);
				if (emptyMethodEntity != null) {
					emptyMethodEntity.setFileName(file.getAbsolutePath());
					emptyMethodEntities.add(emptyMethodEntity);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emptyMethodEntities;
	}

}
