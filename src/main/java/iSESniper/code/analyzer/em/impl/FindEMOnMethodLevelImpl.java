package iSESniper.code.analyzer.em.impl;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.em.FindEMOnMethodLevel;
import iSESniper.code.entity.em.EmptyMethodEntity;

public class FindEMOnMethodLevelImpl implements FindEMOnMethodLevel {

	public EmptyMethodEntity findEmptyMethod(MethodDeclaration methodDeclaration) {
		EmptyMethodEntity emptyMethodEntity = null;
		if (methodDeclaration.toString().indexOf("{") > -1) {
			int methodBodyStartIndex = methodDeclaration.toString().indexOf('{')+1;
			int methodBodyEndIndex = methodDeclaration.toString().lastIndexOf('}');
			String methodBody = methodDeclaration.toString().substring(methodBodyStartIndex, methodBodyEndIndex);
			//去除注释
			methodBody = methodBody.replaceAll("(?<!:)\\/\\/.*|\\/\\*(\\s|.)*?\\*\\/", "");
			//去除空格换行符
			methodBody = methodBody.replaceAll("\\s*","");
			//去除标点
			methodBody = methodBody.replaceAll("\\pP","");
			if (methodBody.length()==0) {
				emptyMethodEntity = new EmptyMethodEntity();
				emptyMethodEntity.setMethodName(methodDeclaration.getName().toString());
				emptyMethodEntity.setRange(methodDeclaration.getRange());
			}
		}
		
		return emptyMethodEntity;
	}

}
