package iSESniper.code.analyzer.em.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import iSESniper.code.analyzer.em.EmptyMethod;
import iSESniper.code.analyzer.em.FindEMOnClassLevel;
import iSESniper.code.entity.em.EmptyMethodEntity;

public class EmptyMethodImpl implements EmptyMethod{

	public List<EmptyMethodEntity> findEmptyMethod(String folderPath) {
		File file  = new File(folderPath);
		List<EmptyMethodEntity> emptyMethodEntities = selectJavaFilesToParse(file);
		return emptyMethodEntities;
	}

	private List<EmptyMethodEntity> selectJavaFilesToParse(File file) {
		File[] files = file.listFiles();
		List<EmptyMethodEntity> emptyMethodEntities = new ArrayList<EmptyMethodEntity>();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				emptyMethodEntities.addAll(selectJavaFilesToParse(files[i]));
			}else if (files[i].getName().endsWith(".java")) {
				File readfile = new File(files[i].getAbsolutePath());
				FindEMOnClassLevel classLevel = new FindEMOnClassLevelImpl();
				emptyMethodEntities.addAll(classLevel.findEmptyMethods(readfile));
			}
		}
		return emptyMethodEntities;
	}

}
