package iSESniper.code.analyzer.em;

import java.io.File;
import java.util.List;

import iSESniper.code.entity.em.EmptyMethodEntity;

public interface FindEMOnClassLevel {
	public List<EmptyMethodEntity>findEmptyMethods(File file);
}
