package iSESniper.code.analyzer.cpd.impl;

import java.util.ArrayList;
import java.util.List;

import iSESniper.code.analyzer.cpd.Cpd;
import iSESniper.code.analyzer.cpd.DuplicatedCodeDetector;
import iSESniper.code.entity.cpd.CpdEntity;
import iSESniper.code.util.system.OS;

public class DuplicatedCodeDetectorImpl implements DuplicatedCodeDetector {

    public List<CpdEntity> findDuplicatedCodeList(String files) {
        OS os = new OS();
        String osName = os.getOsName();
        Cpd cpd = null;
        if (osName.equals("Windows")) {
            cpd = new CpdOnWin();
        } else if (osName.equals("Linux")) {
            cpd = new CpdOnLinux();
        }
        List<CpdEntity> cpdEntities = new ArrayList<>();
        String cpdResult = cpd.excuteCPD(files);
        if (cpdResult.length() == 0 || cpdResult.trim().length() == 0) {
            return cpdEntities;
        } else {
            String[] dcList = cpdResult
                    .split("=====================================================================\n");
            for (String dc : dcList) {
                int line = 0;
                String[] dclines = dc.split("\n");
                int duplicateCodeLineCount = Integer.parseInt(dclines[line].substring(8, dclines[line].indexOf(" line (")));
                line++;
                int duplicateCodeStartLineA = Integer.parseInt(dclines[line].substring(17, dclines[line].indexOf(" of ")));
                String duplicateCodeFileNameA = dclines[line].substring(dclines[line].indexOf(" of ") + 4);
                line++;
                int duplicateCodeStartLineB = Integer.parseInt(dclines[line].substring(17, dclines[line].indexOf(" of ")));
                String duplicateCodeFileNameB = dclines[line].substring(dclines[line].indexOf(" of ") + 4);
               String duplicateCode = "```java  \n";
                for (int i = line; i < dclines.length; i++) {
                    if (dclines[line].startsWith("Starting at line")) {
                        line++;
                    } else {
                        break;
                    }
                }
                for (int i = line; i < dclines.length; i++) {
                    duplicateCode+=dclines[i] + "  \n";
                }
                duplicateCode+="```";
                CpdEntity cpdEntity = new CpdEntity();
                cpdEntity.setDuplicateLineCount(duplicateCodeLineCount);
                cpdEntity.setFileNameA(duplicateCodeFileNameA);
                cpdEntity.setDuplicateCodeStartLineA(duplicateCodeStartLineA);
                cpdEntity.setFileNameB(duplicateCodeFileNameB);
                cpdEntity.setDuplicateCodeStartLineB(duplicateCodeStartLineB);
                cpdEntity.setDuplicateCode(duplicateCode);
                cpdEntities.add(cpdEntity);
            }
            return cpdEntities;
        }

    }

}
