package iSESniper.code.analyzer.cpd.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import iSESniper.code.analyzer.cpd.Cpd;

public class CpdOnLinux implements Cpd {

	public String excuteCPD(String files) {
		// String cpd = "./run.sh cpd --minimum-tokens 70 --files " + files + " --language java";
		String pmdPath = "/opt/GitMining/pmd/pmd-bin-6.4.0/bin";
		String cpd = pmdPath + "/run.sh cpd --minimum-tokens 70 --files " + files + " --language java";
		String cpdResult = "";
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(cpd);
			process.waitFor();
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = null;
			while ((line = inputStream.readLine()) != null) {
				cpdResult += line + "\n";
			}
			inputStream.close();
			process.destroy();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cpdResult;
	}

}
