package iSESniper.code.analyzer.cpd;

import iSESniper.code.entity.cpd.CpdEntity;

import java.util.List;

public interface DuplicatedCodeDetector {
	public List<CpdEntity> findDuplicatedCodeList(String files);
}
