package iSESniper.code.analyzer.logic;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.logic.JavaLogicNodes.java
 *  
 *  2018年2月11日	下午9:31:40  
*/

public interface JavaLogicNodes<T> {
	public T getLogicStatistic(String pth);
}
