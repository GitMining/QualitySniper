package iSESniper.code.analyzer.logic.impl;

import java.util.List;

import com.github.javaparser.ast.stmt.BlockStmt;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.parser.BlkLogicNodeCounter;
import iSESniper.code.parser.itf.ASTBlockTraversal;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.logic.impl.LogicNodeGetter.java
 *  
 *  2018年2月11日	下午6:37:33  
*/

public class LogicNodeGetter {

	public static List<LogicNode> getLogicNodes(BlockStmt n){
		ASTBlockTraversal<LogicNode> blnc = new BlkLogicNodeCounter();
		List<LogicNode> nodes = blnc.getSubBlocks(n);
		return nodes;
	}

}
