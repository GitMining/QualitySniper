package iSESniper.code.analyzer.logic.impl;

import java.util.List;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;

import iSESniper.code.analyzer.logic.LogicStatistic;
import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.parser.JavaASTMethodTool;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.logic.impl.MethodLogicDepth.java
 *  
 *  2018年2月11日	下午6:42:58  
*/

public class MethodLogicDepth implements LogicStatistic {

	public int getLogicStatistic(MethodDeclaration m) {
		BlockStmt bs = JavaASTMethodTool.getMethodBlock(m);
		List<LogicNode> nodes = LogicNodeGetter.getLogicNodes(bs);
		LogicNode node = getDeepestNode(nodes);
		if(node == null) return 1;
		int depth = node.getLayer();
		return depth;
	}
	
	private LogicNode getDeepestNode(List<LogicNode> nodes) {
		if(nodes.size() == 0)	return null;
		LogicNode ans = nodes.get(0);
		for(LogicNode n: nodes) {
			if(n.getLayer() > ans.getLayer())  ans = n;
		}
		return ans;
	}

}
