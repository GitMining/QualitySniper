package iSESniper.code.analyzer.logic.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.logic.JavaLogicNodes;
import iSESniper.code.analyzer.logic.LogicStatistic;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.logic.impl.MethodsCCInJava.java
 *  
 *  2018年2月11日	下午9:30:03  
*/

public class MethodsCCInJava implements JavaLogicNodes<Map<MethodDeclaration, Integer>>{
	
	public Map<String, Integer> getMethodsCC(String pth){
		Map<String, Integer> msCC = new HashMap<String, Integer>();
		Map<MethodDeclaration, Integer> methodsCC = getLogicStatistic(pth);
		for(MethodDeclaration m : methodsCC.keySet()) {
			msCC.put(pth + "." + m.getName().toString(), methodsCC.get(m));
		}
		return msCC;
	}

	public Map<MethodDeclaration, Integer> getLogicStatistic(String pth) {
		LogicStatistic cc = new MethodCyclomaticComplexCounter();
		Map<MethodDeclaration, Integer> methodsCC = new HashMap<MethodDeclaration, Integer>(); 
		try {
			JavaASTTransfer.java2AST(pth);
			if(JavaASTTransfer.interfaceOrNot())
				return methodsCC;
			ArrayList<MethodDeclaration> ms = JavaASTTransfer.getMethodList();
			for(MethodDeclaration m: ms) {
				methodsCC.put(m, cc.getLogicStatistic(m));
			}
			return methodsCC;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	

}
