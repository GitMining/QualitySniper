package iSESniper.code.analyzer.logic.impl;

import java.util.List;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;

import iSESniper.code.analyzer.logic.LogicStatistic;
import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.parser.JavaASTMethodTool;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.cc.impl.MethodCyclomaticComplexCounter.java
 *  
 *  2018年2月10日	下午10:23:51  
*/

public class MethodCyclomaticComplexCounter implements LogicStatistic {

	public int getLogicStatistic(MethodDeclaration m) {
		BlockStmt bs = JavaASTMethodTool.getMethodBlock(m);
		List<LogicNode> nodes = LogicNodeGetter.getLogicNodes(bs);
		return nodes.size()+1;
	}
}
