package iSESniper.code.analyzer.logic.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.logic.JavaLogicNodes;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.logic.impl.MethodsLDInJava.java
 *  
 *  2018年2月11日	下午10:13:31  
*/

public class MethodsLDInJava implements JavaLogicNodes<Map<MethodDeclaration, Integer>> {
	
	public Map<String, Integer> getMethodsLD(String pth){
		Map<String, Integer> msld = new HashMap<String, Integer>();
		Map<MethodDeclaration, Integer> methodsLD = getLogicStatistic(pth);
		for(MethodDeclaration m: methodsLD.keySet()) {
			msld.put(pth + "." + m.getName().toString(), methodsLD.get(m));
		}
		return msld;
	}

	public Map<MethodDeclaration, Integer> getLogicStatistic(String pth) {
		Map<MethodDeclaration, Integer> msld = new HashMap<MethodDeclaration, Integer>();
		MethodLogicDepth mld = new MethodLogicDepth();
		try {
			JavaASTTransfer.java2AST(pth);
			if(JavaASTTransfer.interfaceOrNot())
				return msld;
			ArrayList<MethodDeclaration> ms = JavaASTTransfer.getMethodList();
			for(MethodDeclaration m: ms) {
				msld.put(m, mld.getLogicStatistic(m));
			}
			return msld;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	

}
