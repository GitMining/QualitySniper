package iSESniper.code.analyzer.logic;

import com.github.javaparser.ast.body.MethodDeclaration;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.logic.LogicDepthCountable.java
 *  
 *  2018年2月11日	下午6:42:05  
*/

public interface LogicStatistic {
	public int getLogicStatistic(MethodDeclaration m);
}
