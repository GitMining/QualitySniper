package iSESniper.code.analyzer.loc.impl;

import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.SimpleName;

import iSESniper.code.entity.loc.MethodLinesOfCodeEntity;

public class MethodLOC {

	public static MethodLinesOfCodeEntity getLOC(MethodDeclaration method) {
		String name = method.getName().asString();
		int begin = method.getRange().get().begin.line;
		int end = method.getRange().get().end.line;
		int lines = end-begin;
		String prtClass = "";
		List<Node> nds = method.getParentNode().get().getChildNodes();
		for(Node n: nds) {
			if(n instanceof SimpleName) {
				prtClass = n.toString();
				break;
			}	
		}
		MethodLinesOfCodeEntity mloc = new MethodLinesOfCodeEntity(prtClass, lines, name);
		return mloc;
	}

}
