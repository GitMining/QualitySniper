package iSESniper.code.analyzer.loc.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import iSESniper.code.analyzer.loc.LinesOfCode;
import iSESniper.code.entity.loc.FileLinesOfCodeEntity;

public class FileLOC implements LinesOfCode<FileLinesOfCodeEntity> {

	public FileLinesOfCodeEntity getLOC(String pth) {
		FileLinesOfCodeEntity loc = null;
		long lines = 0;
		long blankLines = 0;
		ArrayList<Long> blankNum = new ArrayList<Long>();
		try {
			FileReader fr = new FileReader(pth);
			BufferedReader br = new BufferedReader(fr);
			try {
				String rl = br.readLine();
				while(rl != null) {
					lines++;
					if(rl.trim().equals("")) {
						blankNum.add(lines);	
						blankLines++;
					}
					rl = br.readLine();
				}	
				loc = new FileLinesOfCodeEntity(pth, lines, blankLines, blankNum);
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			System.out.println("Certain file not fount: " + pth);
			e.printStackTrace();
		}
		return loc;
	}

}
