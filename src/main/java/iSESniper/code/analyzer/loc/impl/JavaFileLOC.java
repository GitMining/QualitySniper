package iSESniper.code.analyzer.loc.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.loc.LinesOfCode;
import iSESniper.code.entity.loc.JavaLinesOfCodeEntity;
import iSESniper.code.entity.loc.MethodLinesOfCodeEntity;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

public class JavaFileLOC implements LinesOfCode<JavaLinesOfCodeEntity> {
	
	public JavaLinesOfCodeEntity getLOC(String pth) {
		long lines = 0;
		int methodNum = 0;
		JavaLinesOfCodeEntity jloc;
		try {
			JavaASTTransfer.java2AST(pth);
			ArrayList<MethodDeclaration>methods = JavaASTTransfer.getMethodList();
			ArrayList<MethodLinesOfCodeEntity> mlocs = new ArrayList<MethodLinesOfCodeEntity>();
			for(MethodDeclaration m: methods) {
				MethodLinesOfCodeEntity mloc = MethodLOC.getLOC(m);
				methodNum++;
				lines += mloc.getLoc();
				mlocs.add(mloc);
			}
			jloc = new JavaLinesOfCodeEntity(pth, lines, methodNum, mlocs);
			return jloc;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
