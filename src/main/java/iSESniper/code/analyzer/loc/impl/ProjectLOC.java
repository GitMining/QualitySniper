package iSESniper.code.analyzer.loc.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import iSESniper.code.analyzer.loc.LinesOfCode;
import iSESniper.code.entity.loc.FileLinesOfCodeEntity;
import iSESniper.code.entity.loc.ModuleLinesOfCodeEntity;
import iSESniper.code.scanner.Scanner;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.scanner.impl.FolderScanner4Java;

public class ProjectLOC implements LinesOfCode<ModuleLinesOfCodeEntity> {

	public ModuleLinesOfCodeEntity getLOC(String location) {
		Scanner sc = new FolderScanner4Java();
		ModuleLinesOfCodeEntity mloc;
		FileLinesOfCodeEntity floc;
		ArrayList<FileLinesOfCodeEntity> flocs = new ArrayList<FileLinesOfCodeEntity>();
		ArrayList<String> pths;
		long lines = 0;
		long blankLines = 0;
		FileLOC fileTool = new FileLOC();
		try {
			pths = sc.scan(location);
			for (String pth : pths) {
				floc = fileTool.getLOC(pth);
				if (floc != null) {
					flocs.add(floc);
					lines += floc.getLoc();
					blankLines += floc.getBlkLoc();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mloc = new ModuleLinesOfCodeEntity(location, lines, blankLines, flocs);
		return mloc ;
	}

}
