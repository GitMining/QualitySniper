package iSESniper.code.analyzer.loc;

public interface LinesOfCode<T> {
	public T getLOC(String pth);
}
