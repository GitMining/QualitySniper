package iSESniper.code.analyzer.loa;

import iSESniper.code.entity.loa.LineOfAnnotateInFilesEntity;

public interface LOAInFolder {
	public LineOfAnnotateInFilesEntity countLineInFolder(String folderPath);
}
