package iSESniper.code.analyzer.loa.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import iSESniper.code.analyzer.loa.LOAInSingleFile;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

public class LOAInSingleFileImpl implements LOAInSingleFile{

	public synchronized LineOfAnnotateInSingleFileEntity statisticsLineNumber(File file) {
		int totalCount = 0; // 总行数
        int annotateCount = 0; // 注释
        int blankCount = 0; // 空格

        boolean flagNode = false;

        String regxNodeBegin = "\\s*/\\*.*"; // 注释正则
        String regxNodeEnd = ".*\\*/\\s*";
        String regx = "//.*";
        String regxSpace = "\\s*";

        BufferedReader br = null;
        try {

            String line = null;
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                totalCount++;
                if (line.matches(regxNodeBegin) && line.matches(regxNodeEnd)) {
                    ++annotateCount;
                }
                if (line.matches(regxNodeBegin)) {
                    ++annotateCount;
                    flagNode = true;
                } else if (line.matches(regxNodeEnd)) {
                    ++annotateCount;
                    flagNode = false;
                } else if (line.matches(regxSpace)) {
                    ++blankCount;
                } else if (line.matches(regx)){
                    ++annotateCount;
                } else if (flagNode){
                    ++annotateCount;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        LineOfAnnotateInSingleFileEntity lOAInSingleFileEntity = new LineOfAnnotateInSingleFileEntity();
        lOAInSingleFileEntity.setFileName(file.getAbsolutePath());
        lOAInSingleFileEntity.setTotalCount(totalCount);
        lOAInSingleFileEntity.setAnnotateCount(annotateCount);
        lOAInSingleFileEntity.setBlankCount(blankCount);
		return lOAInSingleFileEntity;
	}

}
