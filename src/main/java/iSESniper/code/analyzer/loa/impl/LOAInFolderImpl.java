package iSESniper.code.analyzer.loa.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import iSESniper.code.analyzer.loa.LOAInFolder;
import iSESniper.code.analyzer.loa.LOAInSingleFile;
import iSESniper.code.entity.loa.LineOfAnnotateInFilesEntity;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

public class LOAInFolderImpl implements LOAInFolder{

	public LineOfAnnotateInFilesEntity countLineInFolder(String folderPath) {
		File file  = new File(folderPath);
		List<LineOfAnnotateInSingleFileEntity> annotateInSingleFileEntities = countLineForEach(file);
		LineOfAnnotateInFilesEntity lOAnnotateInFilesEntity = new LineOfAnnotateInFilesEntity();
		lOAnnotateInFilesEntity.setFolderPath(file.getPath());
		lOAnnotateInFilesEntity.setLinesOfAnnotate(annotateInSingleFileEntities);
		return lOAnnotateInFilesEntity;
	}

	private List<LineOfAnnotateInSingleFileEntity> countLineForEach(File file) {
		File[] files = file.listFiles();
		List<LineOfAnnotateInSingleFileEntity> annotateInSingleFileEntities = new ArrayList<LineOfAnnotateInSingleFileEntity>();
		
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				annotateInSingleFileEntities.addAll(countLineForEach(files[i]));
			}else if (files[i].getName().endsWith(".java")) {
				File readfile = new File(files[i].getAbsolutePath());
				LOAInSingleFile loaInSingleFile = new LOAInSingleFileImpl();
				annotateInSingleFileEntities.add(loaInSingleFile.statisticsLineNumber(readfile));
			}
		}
		
		return annotateInSingleFileEntities;
	}

}
