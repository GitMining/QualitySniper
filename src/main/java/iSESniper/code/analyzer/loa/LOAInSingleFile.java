package iSESniper.code.analyzer.loa;

import java.io.File;

import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

public interface LOAInSingleFile {
	public LineOfAnnotateInSingleFileEntity statisticsLineNumber(File file);
}
