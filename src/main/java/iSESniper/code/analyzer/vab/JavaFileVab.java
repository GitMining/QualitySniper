package iSESniper.code.analyzer.vab;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.vab.JavaFileVab.java
 *  
 *  2018年2月8日	下午10:32:16  
*/

public interface JavaFileVab<T> {
	public T getVabStatistic(String pth);
}
