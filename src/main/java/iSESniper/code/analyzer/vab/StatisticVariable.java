package iSESniper.code.analyzer.vab;

import com.github.javaparser.ast.body.MethodDeclaration;

public interface StatisticVariable<T> {
    public T getVariableCount(MethodDeclaration method);
}
