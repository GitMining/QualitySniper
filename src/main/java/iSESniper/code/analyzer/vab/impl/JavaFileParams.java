package iSESniper.code.analyzer.vab.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.vab.JavaFileVab;
import iSESniper.code.entity.vab.MethodParameters;
import iSESniper.code.entity.vab.MethodParamsInFile;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.variable.impl.JavaFileParams.java
 *  
 *  2018年2月8日	下午10:30:18  
*/

public class JavaFileParams implements JavaFileVab<MethodParamsInFile> {

	public MethodParamsInFile getVabStatistic(String pth) {
		MethodParams mp = new MethodParams();
		MethodParamsInFile mpf;
		ArrayList<MethodParameters> mpList = new ArrayList<MethodParameters>(); 
		try {
			JavaASTTransfer.java2AST(pth);
			ArrayList<MethodDeclaration> mds = JavaASTTransfer.getMethodList();
			for(MethodDeclaration m: mds) {
				mpList.add(mp.getVariableCount(m));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mpf = new MethodParamsInFile(pth, mpList);
		return mpf;
	}

}
