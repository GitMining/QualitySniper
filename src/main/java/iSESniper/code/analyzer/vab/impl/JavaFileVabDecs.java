package iSESniper.code.analyzer.vab.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.github.javaparser.ast.body.MethodDeclaration;

import iSESniper.code.analyzer.vab.JavaFileVab;
import iSESniper.code.entity.vab.MethodVabDeclarations;
import iSESniper.code.entity.vab.MethodVabsInFile;
import iSESniper.code.parser.JavaASTTransfer;
import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.variable.impl.JavaFileVabDecs.java
 *  
 *  2018年2月8日	下午11:38:30  
*/

public class JavaFileVabDecs implements JavaFileVab<MethodVabsInFile> {

	public MethodVabsInFile getVabStatistic(String pth) {
		MethodVabs mv = new MethodVabs();
		MethodVabsInFile mvf;
		ArrayList<MethodVabDeclarations> mvdList = new ArrayList<MethodVabDeclarations>(); 
		try {
			JavaASTTransfer.java2AST(pth);
			if(JavaASTTransfer.interfaceOrNot())
				return null;
			ArrayList<MethodDeclaration> mds = JavaASTTransfer.getMethodList();
			for(MethodDeclaration m: mds) {
				mvdList.add(mv.getVariableCount(m));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileIsNotJavaTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mvf = new MethodVabsInFile(pth, mvdList);
		return mvf;
	}

}
