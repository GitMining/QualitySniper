package iSESniper.code.analyzer.vab.impl;

import java.util.ArrayList;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;

import iSESniper.code.analyzer.vab.StatisticVariable;
import iSESniper.code.entity.vab.MethodParameters;
import iSESniper.code.entity.vab.Variable;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.variable.impl.MethodParams.java
 *  
 *  2018年2月7日	下午5:49:26  
*/

public class MethodParams implements StatisticVariable<MethodParameters>{

	public MethodParameters getVariableCount(MethodDeclaration method) {
		MethodParameters mParams;
		NodeList<Parameter> params = method.getParameters();
		ArrayList<Variable> vabs = new ArrayList<Variable>();
		for(Parameter p: params) {
			String type = p.getType().toString(); 
			String name = p.getName().toString();
			vabs.add(new Variable(type, name));
		}
		mParams = new MethodParameters(method, vabs);
		return mParams;
	}

}
