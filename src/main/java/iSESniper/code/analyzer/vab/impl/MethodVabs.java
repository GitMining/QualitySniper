package iSESniper.code.analyzer.vab.impl;

import java.util.ArrayList;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;

import iSESniper.code.analyzer.vab.StatisticVariable;
import iSESniper.code.entity.vab.DeclarationVariable;
import iSESniper.code.entity.vab.MethodVabDeclarations;
import iSESniper.code.parser.BlockVabDeclarationCounter;
import iSESniper.code.parser.JavaASTMethodTool;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.analyzer.variable.impl.MethodVabriables.java
 *  
 *  2018年2月7日	下午7:50:38  
*/

public class MethodVabs implements StatisticVariable<MethodVabDeclarations> {

	public MethodVabDeclarations getVariableCount(MethodDeclaration method) {
		BlockVabDeclarationCounter bvdc = new BlockVabDeclarationCounter();
		BlockStmt bs = JavaASTMethodTool.getMethodBlock(method);
		ArrayList<DeclarationVariable> decs = bvdc.getSubBlocks(bs);
		MethodVabDeclarations mvd = new MethodVabDeclarations(method, decs);
		return mvd;
	}

}
