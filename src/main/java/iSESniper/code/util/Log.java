package iSESniper.code.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.util.Log.java
 *  
 *  2018年3月7日	下午7:42:42  
*/

public class Log {
	public static boolean init() {
		File f = new File("LogOutput");
		if(f.exists())	return false;
		else {
			if(f.mkdirs())	return true;
		}
		return false;
	}
	
	public static boolean createLog(String name, List<String> content) throws IOException {
		long t = new Date().getTime();
		File f = new File("LogOutput/"+name+t+".log");
		File fp = new File(f.getParent());
		if(!fp.exists())  return false;
		f.createNewFile();
		try(FileOutputStream fos = new FileOutputStream(f);
				OutputStreamWriter writer = new OutputStreamWriter(fos)){
			for(String s: content) {
				writer.append(s);
			}
		}
		return true;
	}
}
