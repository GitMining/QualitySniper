package iSESniper.code.util.system;

import java.util.Properties;

/**
 * @author VousAttendezrer
 * 
 *         This file contains some information of current operation system.
 */
public class OS {
	private String osName;

	/**
	 * @return the name of current operation system.
	 */
	public String getOsName() {
		Properties properties = System.getProperties();
		String osName = properties.getProperty("os.name").toLowerCase();
		if (osName.indexOf("win") > -1) {
			this.setOsName("Windows");
		} else if (osName.indexOf("linux") > -1) {
			this.setOsName("Linux");
		}
		return this.osName;
	}

	private void setOsName(String osName) {
		this.osName = osName;
	}

}
