package iSESniper.code.util.enumerate;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.util.Modifiers.java
 *  
 *  2018年2月7日	下午5:28:44  
*/

public enum Modifiers {
	PUBLIC, PROTECTED, PRIVATE, VACANTMODIFIER;
}
