package iSESniper.code.util.entity;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.util.entity.Position.java
 *  
 *  2018年2月7日	下午7:22:32  
*/

public class Position {
	private int line;
	private int col;
	
	public Position(int line, int col) {
		super();
		this.line = line;
		this.col = col;
	}

	public int getLine() {
		return line;
	}

	public int getCol() {
		return col;
	}

	@Override
	public String toString() {
		return "[" + line + " line, " + col + " col]";
	}
}
