package iSESniper.code.util.entity;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.util.entity.Location.java
 *  
 *  2018年2月7日	下午7:21:45  
*/

public class Location {
	private String pth;
	private Position begin;
	private Position end;
	
	public Location(String pth, Position begin, Position end) {
		super();
		this.pth = pth;
		this.begin = begin;
		this.end = end;
	}

	public String getPth() {
		return pth;
	}

	public Position getBegin() {
		return begin;
	}

	public Position getEnd() {
		return end;
	}

	@Override
	public String toString() {
		return "[Location] " + begin.toString() + ", " + end.toString();
	}
}
