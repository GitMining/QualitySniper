package iSESniper.code.parser;

import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;

import iSESniper.code.util.entity.Location;
import iSESniper.code.util.entity.Position;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.parser.JavaASTMethodTool.java
 *  
 *  2018年2月7日	下午10:58:26  
*/

public class JavaASTMethodTool {
	public static BlockStmt getMethodBlock(MethodDeclaration method){
		List<Node> children = method.getChildNodes();
		for(Node child: children) {
			if(child instanceof BlockStmt) return (BlockStmt) child;
		}
		return null;
	}
	
	public static Location getNodeLocation(Node n) {
		int bc = n.getRange().get().begin.column;
		int bl = n.getRange().get().begin.line;
		int ec = n.getRange().get().end.column;
		int el = n.getRange().get().end.line;
		Location nLoc = new Location(n.toString(), new Position(bl, bc), new Position(el, ec));
		return nLoc;
	}
	
}
