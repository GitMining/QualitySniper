package iSESniper.code.parser.exception;

public class FileIsNotJavaTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FileIsNotJavaTypeException(String e) {
		super(e+"\n Target file is not java file.");
	}
	
	public FileIsNotJavaTypeException() {
		
	}
}
