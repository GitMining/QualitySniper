package iSESniper.code.parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import iSESniper.code.parser.exception.FileIsNotJavaTypeException;

public class JavaASTTransfer {

	private static CompilationUnit cu;
	
	private static boolean isInterface;
	
	public static void java2AST(String pth) throws FileNotFoundException, FileIsNotJavaTypeException {
		cu = getCU(pth);
		setIsInter();
	}
	
	public static boolean interfaceOrNot() {
		return isInterface;
	}

	// Visiting class methods
	public static ArrayList<MethodDeclaration> getMethodList() throws Exception {
		ArrayList<MethodDeclaration> methodList= new ArrayList<MethodDeclaration>();
		new MethodVisitor().visit(cu, methodList);
		return methodList;
	}

	/**
	 * Simple visitor implementation for visiting MethodDeclaration nodes.
	 */
	private static class MethodVisitor extends VoidVisitorAdapter<Object> {
		@SuppressWarnings("unchecked")
		@Override
		public void visit(MethodDeclaration n, Object arg) {
			ArrayList<MethodDeclaration> methodList = new ArrayList<MethodDeclaration>();
			methodList = (ArrayList<MethodDeclaration>) arg;
			methodList.add(n);
		}
	}

	private static CompilationUnit getCU(String pth) throws FileNotFoundException, FileIsNotJavaTypeException {
		String regEx = "\\.java$";
		Pattern ptn = Pattern.compile(regEx);
		Matcher mtch = ptn.matcher(pth);
		if(!mtch.find()) throw new FileIsNotJavaTypeException();
		
		// Create file input stream for the file to be parsed.
		FileInputStream in = new FileInputStream(pth);

		// parse the file.
		CompilationUnit cu = JavaParser.parse(in);

		return cu;
	}
	
	private static void setIsInter() {
		List<Node> children = cu.getChildNodes();
		for(Node n: children) {
			if(n instanceof ClassOrInterfaceDeclaration) {
				isInterface = ((ClassOrInterfaceDeclaration) n).isInterface();
			}
		}
	}
}
