package iSESniper.code.parser;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.VariableDeclarator;

import iSESniper.code.entity.vab.DeclarationVariable;
import iSESniper.code.parser.itf.ASTBlockTraversal;
import iSESniper.code.util.entity.Location;
import iSESniper.code.util.entity.Position;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.parser.BlockVabDeclarationCounter.java
 *  
 *  2018年2月8日	下午6:13:36  
*/

public class BlockVabDeclarationCounter implements ASTBlockTraversal<DeclarationVariable> {
	
	private int vabDecCount = 0;
	private ArrayList<DeclarationVariable> vabs = new ArrayList<DeclarationVariable>();
	
	public ArrayList<DeclarationVariable> getSubBlocks(Node bs) {
		ArrayList<Node> ns = new ArrayList<Node>();
		getVabDecl(bs, ns);
		for(Node n: ns) {
			int bc = n.getRange().get().begin.column;
			int bl = n.getRange().get().begin.line;
			int ec = n.getRange().get().end.column;
			int el = n.getRange().get().end.line;
			Location nLoc = new Location(n.toString(), new Position(bl, bc), new Position(el, ec));
			VariableDeclarator temp = (VariableDeclarator) n;
			String type = temp.getType().toString();
			String name = temp.getName().toString();
			vabs.add(new DeclarationVariable(type, name, nLoc));
		}
		this.vabDecCount = this.vabs.size();
		return this.vabs;
	}
	
	private void getVabDecl(Node bs, ArrayList<Node> ns) {
		if(bs == null)
			return;
		List<Node> children = bs.getChildNodes();
		for(Node c: children) {
			if(c instanceof VariableDeclarator)
				ns.add(c);
			getVabDecl(c, ns);
		}
	}

	public int getVabDecCount() {
		return vabDecCount;
	}

}
