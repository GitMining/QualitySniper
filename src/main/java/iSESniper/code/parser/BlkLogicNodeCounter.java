package iSESniper.code.parser;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.stmt.DoStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import com.github.javaparser.ast.stmt.ForeachStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.SwitchEntryStmt;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.stmt.WhileStmt;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.parser.itf.ASTBlockTraversal;
import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.parser.BlkLogicNodeCounter.java
 *  
 *  2018年2月11日	下午2:29:49  
*/

public class BlkLogicNodeCounter implements ASTBlockTraversal<LogicNode> {

	public List<LogicNode> getSubBlocks(Node n) {
		 List<LogicNode> nodes = new ArrayList<LogicNode>();
		 getAllLogicNodes(n, nodes, 1, 0);
		return nodes;
	}
	
	private void getAllLogicNodes(Node n, List<LogicNode> nodes, int layer, Integer nodeId) {
		if (n == null)
			return;
		List<Node> children = n.getChildNodes();
		for(Node child: children) {
			if(isLogicNode(child)) {
				nodeId = nodes.size() + 1;
				Location nLoc = JavaASTMethodTool.getNodeLocation(child);
				nodes.add(new LogicNode(nLoc, layer+1, nodeId));
				getAllLogicNodes(child, nodes, layer+1, nodeId);
			}
			if(isSwitch(child)) getAllLogicNodes(child, nodes, layer, nodeId);
		}
	}
	
	private boolean isLogicNode(Node n) {
		boolean flag = false;
		if(n instanceof ForStmt || n instanceof WhileStmt || n instanceof DoStmt || n instanceof ForeachStmt
				|| n instanceof IfStmt || n instanceof SwitchEntryStmt) {
			flag = true;
			if(n instanceof SwitchEntryStmt) {
				SwitchEntryStmt temp = (SwitchEntryStmt) n;
				if(temp.getLabel().toString().equals("Optional.empty"))	flag = false;
			}
		}
		return flag;
	}
	
	private boolean isSwitch(Node n) {
		boolean flag = false;
		if(n instanceof SwitchStmt) {
			flag = true;
		}
		return flag;
	}
}
