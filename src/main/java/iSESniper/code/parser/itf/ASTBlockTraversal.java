package iSESniper.code.parser.itf;

import java.util.List;

import com.github.javaparser.ast.Node;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.parser.itf.ASTTraversal.java
 *  
 *  2018年2月8日	下午6:02:51  
*/

public interface ASTBlockTraversal<T> {
	public List<T> getSubBlocks(Node n);
}
