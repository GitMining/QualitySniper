package iSESniper.code.entity.loc;

public class LinesOfCodeEntity {
	private String pth;
	private long lines;
	private long blankLines;

	public LinesOfCodeEntity(String pth, long loc, long blkLoc) {
		super();
		this.pth = pth;
		this.lines = loc;
		this.blankLines = blkLoc;
	}
	
	public LinesOfCodeEntity(String pth, long lines) {
		super();
		this.pth = pth;
		this.lines = lines;
	}

	public String getPth() {
		return pth;
	}

	public long getLoc() {
		return lines;
	}

	public long getBlkLoc() {
		return blankLines;
	}

	@Override
	public String toString() {
		return "[LinesOfCode] module_name= [" + pth + "], lines=" + lines + ", blankLines=" + blankLines + "\n";
	}
}
