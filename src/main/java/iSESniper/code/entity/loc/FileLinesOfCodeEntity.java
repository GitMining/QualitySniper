package iSESniper.code.entity.loc;

import java.util.ArrayList;

public class FileLinesOfCodeEntity extends LinesOfCodeEntity {
	private ArrayList<Long> blankNum;

	public FileLinesOfCodeEntity(String pth, long loc, long blkLoc) {
		super(pth, loc, blkLoc);
	}
	
	public FileLinesOfCodeEntity(String pth, long loc, long blkLoc, ArrayList<Long> blkNum) {
		super(pth, loc, blkLoc);
		this.blankNum = blkNum;
	}

	public ArrayList<Long> getBlankNum() {
		return blankNum;
	}

	@Override
	public String toString() {
		return super.toString() + "each blank number is:" + blankNum.toString() + "\n";
	}
    
}
