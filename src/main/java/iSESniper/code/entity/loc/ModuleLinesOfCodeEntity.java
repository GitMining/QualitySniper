package iSESniper.code.entity.loc;

import java.util.ArrayList;

public class ModuleLinesOfCodeEntity extends LinesOfCodeEntity {
	
	private ArrayList<FileLinesOfCodeEntity> subModules;
	
	
	public ModuleLinesOfCodeEntity(String pth, long loc, long blkLoc) {
		super(pth, loc, blkLoc);
	}

	public ModuleLinesOfCodeEntity(String pth, long loc, long blkLoc,
			ArrayList<FileLinesOfCodeEntity> subModules) {
		super(pth, loc, blkLoc);
		this.subModules = subModules;
	}	
	
	public ArrayList<FileLinesOfCodeEntity> getSubModules() {
		return subModules;
	}

	@Override
	public String toString() {
		return super.toString() + "[ModuleLinesOfCode]\n" + subModules.toString() + "\n";
	}

}
