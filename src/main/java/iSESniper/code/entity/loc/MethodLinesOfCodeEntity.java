package iSESniper.code.entity.loc;

public class MethodLinesOfCodeEntity extends LinesOfCodeEntity {

	private String methodName;
	
	public MethodLinesOfCodeEntity(String pth, long loc) {
		super(pth, loc, 0);
	}

	public MethodLinesOfCodeEntity(String pth, long loc, String methodName) {
		super(pth, loc, 0);
		this.methodName = methodName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	@Override
	@Deprecated
	public long getBlkLoc() {
		return 0;
	}

	@Override
	public String toString() {
		return "[MethodLinesOfCodeEntity] [methodName=" + methodName + ", getPth()=" + getPth() + ", getLoc()=" + getLoc()
				+ "]\n";
	}
	
	
}
