package iSESniper.code.entity.loc;

import java.util.ArrayList;

public class JavaLinesOfCodeEntity extends LinesOfCodeEntity {

	private int methodNum;
	
	private ArrayList<MethodLinesOfCodeEntity> methodList;

	public JavaLinesOfCodeEntity(String pth, long loc) {
		super(pth, loc);
	}

	public JavaLinesOfCodeEntity(String pth, long loc, int methodNum, ArrayList<MethodLinesOfCodeEntity> methodList) {
		super(pth, loc);
		this.methodNum = methodNum;
		this.methodList = methodList;
	}

	public int getMethodNum() {
		return methodNum;
	}

	public ArrayList<MethodLinesOfCodeEntity> getMethodList() {
		return methodList;
	}

	@Override
	public String toString() {
		return "[JavaLinesOfCodeEntity] FullName: " + getPth() + ", methodNumber: " + methodNum + ", methodList=" + methodList
				+ ", getLoc()=" + getLoc() + "\n";
	}
	
	
}
