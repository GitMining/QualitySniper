package iSESniper.code.entity.vab;

/**
 * 2018/2/7
 * @author Magister
 *
 */
public class Variable {
	
	private String clazz;
	
	private String vName;

	public Variable(String clazz, String vName) {
		super();
		this.clazz = clazz;
		this.vName = vName;
	}

	public String getClazz() {
		return clazz;
	}

	public String getvName() {
		return vName;
	}

	@Override
	public String toString() {
		return "[Variable] " + " " + clazz + " " + vName;
	}
	
}
