package iSESniper.code.entity.vab;

import java.util.ArrayList;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.variable.MethodVabsInFile.java
 *  
 *  2018年2月8日	下午11:35:15  
*/

public class MethodVabsInFile {
	
	private String pth;
	
	private ArrayList<MethodVabDeclarations> mvdList;

	public String getPth() {
		return pth;
	}

	public void setPth(String pth) {
		this.pth = pth;
	}

	public ArrayList<MethodVabDeclarations> getMvdList() {
		return mvdList;
	}

	public void setMvdList(ArrayList<MethodVabDeclarations> mvdList) {
		this.mvdList = mvdList;
	}

	public MethodVabsInFile(String pth, ArrayList<MethodVabDeclarations> mvdList) {
		super();
		this.pth = pth;
		this.mvdList = mvdList;
	}

	@Override
	public String toString() {
		return pth + "\n" + mvdList;
	}
	
}
