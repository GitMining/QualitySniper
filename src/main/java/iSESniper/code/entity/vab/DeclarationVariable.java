package iSESniper.code.entity.vab;

import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.variable.DeclarationVariable.java
 *  
 *  2018年2月7日	下午7:18:14  
*/

public class DeclarationVariable extends Variable {

	private Location location; 

	public DeclarationVariable(String clazz, String vName) {
		super(clazz, vName);
	}
	
	public DeclarationVariable(String clazz, String vName, Location location) {
		super(clazz, vName);
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	@Override
	public String toString() {
		return  super.toString() + " location:" + location.toString();
	}
	
}
