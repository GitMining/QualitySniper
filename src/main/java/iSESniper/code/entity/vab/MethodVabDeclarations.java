package iSESniper.code.entity.vab;

import java.util.ArrayList;

import com.github.javaparser.ast.body.MethodDeclaration;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.variable.MethodDeclarations.java
 *  
 *  2018年2月7日	下午7:33:50  
*/

public class MethodVabDeclarations {
	
	private MethodDeclaration method;
	
	private ArrayList<DeclarationVariable> decVabs;

	public MethodVabDeclarations(MethodDeclaration method, ArrayList<DeclarationVariable> decs) {
		super();
		this.method = method;
		this.decVabs = decs;
	}

	public MethodDeclaration getMethod() {
		return method;
	}

	public ArrayList<DeclarationVariable> getDecs() {
		return decVabs;
	}
	
	public int getVabsCount() {
		return this.decVabs.size();
	}
	
	public String getMethodName() {
		return method.getName().toString();
	}

	@Override
	public String toString() {
		return "[MethodVabDeclarations] " + method.getName() + "\nVariable Decrlaration: " + decVabs + "\n";
	}
	
}
