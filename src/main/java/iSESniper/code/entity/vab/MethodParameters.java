package iSESniper.code.entity.vab;

import java.util.ArrayList;

import com.github.javaparser.ast.body.MethodDeclaration;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.variable.MethodParameters.java
 *  
 *  2018年2月7日	下午5:15:04  
*/

public class MethodParameters {
	
	private MethodDeclaration parentNode;
	
	private ArrayList<Variable> params;

	public MethodParameters(MethodDeclaration parentNode, ArrayList<Variable> params) {
		super();
		this.parentNode = parentNode;
		this.params = params;
	}

	public MethodDeclaration getParentNode() {
		return parentNode;
	}

	public ArrayList<Variable> getParams() {
		return params;
	}
	
	public String getMethodName() {
		return parentNode.getName().toString();
	}

	@Override
	public String toString() {
		return "[MethodParameters] Method: " + parentNode.getName().asString() + "; params=" + params + "\n";
	}
}
