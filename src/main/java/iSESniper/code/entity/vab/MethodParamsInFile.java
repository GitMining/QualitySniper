package iSESniper.code.entity.vab;

import java.util.ArrayList;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.variable.MethodParamsInFile.java
 *  
 *  2018年2月8日	下午10:36:44  
*/

public class MethodParamsInFile {
	
	private String pth;
	
	private ArrayList<MethodParameters> msParams;
	
	public MethodParamsInFile(String pth, ArrayList<MethodParameters> msParams) {
		super();
		this.pth = pth;
		this.msParams = msParams;
	}
	
	public String getPth() {
		return pth;
	}
	
	public void setPth(String pth) {
		this.pth = pth;
	}
	
	public ArrayList<MethodParameters> getMsParams() {
		return msParams;
	}
	
	public void setMsParams(ArrayList<MethodParameters> msParams) {
		this.msParams = msParams;
	}
	
	@Override
	public String toString() {
		return pth + "\n" + msParams;
	}
	
}
