package iSESniper.code.entity.cpd;

public class CpdEntity {
	private int duplicateLineCount;
	private String fileNameA;
	private int duplicateCodeStartLineA;
	private String fileNameB;
	private int duplicateCodeStartLineB;
	private String duplicateCode;

	public int getDuplicateLineCount() {
		return duplicateLineCount;
	}

	public void setDuplicateLineCount(int duplicateLineCount) {
		this.duplicateLineCount = duplicateLineCount;
	}

	public String getFileNameA() {
		return fileNameA;
	}

	public void setFileNameA(String fileNameA) {
		this.fileNameA = fileNameA;
	}

	public int getDuplicateCodeStartLineA() {
		return duplicateCodeStartLineA;
	}

	public void setDuplicateCodeStartLineA(int duplicateCodeStartLineA) {
		this.duplicateCodeStartLineA = duplicateCodeStartLineA;
	}

	public String getFileNameB() {
		return fileNameB;
	}

	public void setFileNameB(String fileNameB) {
		this.fileNameB = fileNameB;
	}

	public int getDuplicateCodeStartLineB() {
		return duplicateCodeStartLineB;
	}

	public void setDuplicateCodeStartLineB(int duplicateCodeStartLineB) {
		this.duplicateCodeStartLineB = duplicateCodeStartLineB;
	}


	public String getDuplicateCode() {
		return duplicateCode;
	}

	public void setDuplicateCode(String duplicateCode) {
		this.duplicateCode = duplicateCode;
	}

	@Override
	public String toString() {
		return this.getDuplicateLineCount() + "\n" + this.getDuplicateCodeStartLineA() + "\n" + this.getFileNameA()
				+ "\n" + this.getDuplicateCodeStartLineB() + "\n" + this.getFileNameB() + "\n"
				+ this.getDuplicateCode();
	}
}
