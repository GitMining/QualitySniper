package iSESniper.code.entity.loa;

public class LineOfAnnotateInSingleFileEntity {
	private int totalCount;
	private int annotateCount;
	private int blankCount;
	private String fileName;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getAnnotateCount() {
		return annotateCount;
	}

	public void setAnnotateCount(int annotateCount) {
		this.annotateCount = annotateCount;
	}

	public int getBlankCount() {
		return blankCount;
	}

	public void setBlankCount(int blankCount) {
		this.blankCount = blankCount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "Total count of lines:" + this.totalCount + ", count of annotate lines:" + this.annotateCount
				+ ", count of blank lines:" + this.blankCount + ". File name:" + this.fileName;
	}
}
