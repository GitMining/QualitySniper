package iSESniper.code.entity.loa;

import java.util.List;

public class LineOfAnnotateInFilesEntity {
	private List<LineOfAnnotateInSingleFileEntity> linesOfAnnotate;
	private String folderPath;

	public List<LineOfAnnotateInSingleFileEntity> getLinesOfAnnotate() {
		return linesOfAnnotate;
	}

	public void setLinesOfAnnotate(List<LineOfAnnotateInSingleFileEntity> linesOfAnnotate) {
		this.linesOfAnnotate = linesOfAnnotate;
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	@Override
	public String toString() {
		int totalCount = 0, annotateCount = 0, blankCount = 0;
		for (int i = 0; i < linesOfAnnotate.size(); i++) {
			LineOfAnnotateInSingleFileEntity annotateInSingleFileEntity = linesOfAnnotate.get(i);
			totalCount += annotateInSingleFileEntity.getTotalCount();
			annotateCount += annotateInSingleFileEntity.getAnnotateCount();
			blankCount += annotateInSingleFileEntity.getBlankCount();
		}
		return "Folder path:" + this.folderPath + " . Total count of lines:" + totalCount + ", count of annotate lines:"
				+ annotateCount + ", count of blank lines:" + blankCount + ".";
	}
}
