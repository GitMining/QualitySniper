package iSESniper.code.entity.logic.circle;

import java.util.List;

import com.github.javaparser.ast.Node;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.circle.DoLogicNode.java
 *  
 *  2018年2月11日	下午1:09:05  
*/

public class DoLogicNode extends LogicNode {
	public DoLogicNode(List<Node> stmts, List<LogicNode> subNodes, Location location, int layer, int nodeId) {
		super(stmts, subNodes, location, layer, nodeId);
	}

	public DoLogicNode(List<Node> stmts, Location location, int layer, int nodeId) {
		super(stmts, location, layer, nodeId);
	}
	
	public DoLogicNode(Location location, int layer, int nodeId) {
		super(location, layer, nodeId);
	}
}
