package iSESniper.code.entity.logic.circle;

import java.util.List;

import com.github.javaparser.ast.Node;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.circle.ForLogicNode.java
 *  
 *  2018年2月11日	下午1:11:57  
*/

public class ForLogicNode extends LogicNode {

	public ForLogicNode(List<Node> stmts, List<LogicNode> subNodes, Location location, int layer, int nodeId) {
		super(stmts, subNodes, location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public ForLogicNode(List<Node> stmts, Location location, int layer, int nodeId) {
		super(stmts, location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public ForLogicNode(Location location, int layer, int nodeId) {
		super(location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}
		
}
