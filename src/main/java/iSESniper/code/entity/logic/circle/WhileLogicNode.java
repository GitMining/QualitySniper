package iSESniper.code.entity.logic.circle;

import java.util.List;

import com.github.javaparser.ast.Node;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.circle.WhileLogicNode.java
 *  
 *  2018年2月10日	下午11:11:37  
*/

public class WhileLogicNode extends LogicNode {

	public WhileLogicNode(List<Node> stmts, List<LogicNode> subNodes, Location location, int layer, int nodeId) {
		super(stmts, subNodes, location, layer, nodeId);
	}

	public WhileLogicNode(List<Node> stmts, Location location, int layer, int nodeId) {
		super(stmts, location, layer, nodeId);
	}
	
	public WhileLogicNode(Location location, int layer, int nodeId) {
		super(location, layer, nodeId);
	}
}
