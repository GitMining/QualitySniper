package iSESniper.code.entity.logic;

import java.util.List;

import com.github.javaparser.ast.body.MethodDeclaration;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.MethodCC.java
 *  
 *  2018年2月11日	下午9:43:53  
*/

public class MethodCC {

	private List<LogicNode> nodes;
	
	private MethodDeclaration method;

	public MethodCC(List<LogicNode> nodes, MethodDeclaration method) {
		super();
		this.nodes = nodes;
		this.method = method;
	}

	public List<LogicNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<LogicNode> nodes) {
		this.nodes = nodes;
	}

	public MethodDeclaration getMethod() {
		return method;
	}

	public void setMethod(MethodDeclaration method) {
		this.method = method;
	}

	@Override
	public String toString() {
		return "[MethodCC] " + method.getName().toString() + " " + nodes;
	}
	
}
