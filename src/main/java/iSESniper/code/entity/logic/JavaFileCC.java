package iSESniper.code.entity.logic;

import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.JavaFileCC.java
 *  
 *  2018年2月11日	下午9:43:11  
*/

public class JavaFileCC {
	
	private String pth;
	
	private List<MethodCC> methods;

	public String getPth() {
		return pth;
	}

	public void setPth(String pth) {
		this.pth = pth;
	}

	public List<MethodCC> getMethods() {
		return methods;
	}

	public void setMethods(List<MethodCC> methods) {
		this.methods = methods;
	}

	public JavaFileCC(String pth, List<MethodCC> methods) {
		super();
		this.pth = pth;
		this.methods = methods;
	}

	@Override
	public String toString() {
		return "[JavaFileCC] " + pth + " \n" + methods;
	}
}
