package iSESniper.code.entity.logic.branch;

import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.branch.SwitchLogicNode.java
 *  
 *  2018年2月11日	下午1:40:40  
*/

public class SwitchLogicNode{
	
	private List<SwitchBranchLogicNode> subNodes;

	public SwitchLogicNode(List<SwitchBranchLogicNode> subNodes) {
		super();
		this.subNodes = subNodes;
	}

	public List<SwitchBranchLogicNode> getSubNodes() {
		return subNodes;
	}

	public void setSubNodes(List<SwitchBranchLogicNode> subNodes) {
		this.subNodes = subNodes;
	}

	
}
