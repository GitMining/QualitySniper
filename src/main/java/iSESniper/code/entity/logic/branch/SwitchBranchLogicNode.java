package iSESniper.code.entity.logic.branch;

import java.util.List;

import com.github.javaparser.ast.Node;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.branch.SwitchBranchLogicNode.java
 *  
 *  2018年2月11日	下午1:49:15  
*/

public class SwitchBranchLogicNode extends LogicNode {

	public SwitchBranchLogicNode(Location location, int layer, int nodeId) {
		super(location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public SwitchBranchLogicNode(List<Node> stmts, Location location, int layer, int nodeId) {
		super(stmts, location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public SwitchBranchLogicNode(List<Node> stmts, List<LogicNode> subNodes, Location location, int layer, int nodeId) {
		super(stmts, subNodes, location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

}
