package iSESniper.code.entity.logic.branch;

import java.util.List;

import com.github.javaparser.ast.Node;

import iSESniper.code.entity.logic.LogicNode;
import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.branch.IfLogicNode.java
 *  
 *  2018年2月11日	下午1:26:24  
*/

public class IfLogicNode extends LogicNode {
	
	private List<IfLogicNode> broodIfs;

	public IfLogicNode(List<Node> stmts, List<LogicNode> subNodes, Location location, int layer, int nodeId) {
		super(stmts, subNodes, location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public IfLogicNode(List<Node> stmts, Location location, int layer, int nodeId) {
		super(stmts, location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public IfLogicNode(Location location, int layer, int nodeId) {
		super(location, layer, nodeId);
		// TODO Auto-generated constructor stub
	}

	public IfLogicNode(Location location, int layer, int nodeId, List<IfLogicNode> broodIfs) {
		super(location, layer, nodeId);
		this.broodIfs = broodIfs;
	}

	public List<IfLogicNode> getBroodIfs() {
		return broodIfs;
	}

	public void setBroodIfs(List<IfLogicNode> broodIfs) {
		this.broodIfs = broodIfs;
	}

}
