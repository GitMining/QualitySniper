package iSESniper.code.entity.logic;

import java.util.List;

import com.github.javaparser.ast.Node;

import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.entity.logic.LogicNode.java
 *  
 *  2018年2月10日	下午10:33:40  
*/

public class LogicNode {
	
	//Contents included in LogicNode.
	private List<Node> stmts;
	
	//SubNodes which is also the kind of LogicNdoe belong to this node.
	private List<LogicNode> subNodes;
	
	//This node's location.
	private Location location;
	
	//The layer that the node belong to.
	private int layer;
	
	//The node's id at its layer.
	private int nodeId;

	public List<Node> getStmts() {
		return stmts;
	}

	public void setStmts(List<Node> stmts) {
		this.stmts = stmts;
	}

	public List<LogicNode> getSubNodes() {
		return subNodes;
	}

	public void setSubNodes(List<LogicNode> subNodes) {
		this.subNodes = subNodes;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public LogicNode(Location location, int layer, int nodeId) {
		super();
		this.location = location;
		this.layer = layer;
		this.nodeId = nodeId;
	}

	public LogicNode(List<Node> stmts, Location location, int layer, int nodeId) {
		super();
		this.stmts = stmts;
		this.location = location;
		this.layer = layer;
		this.nodeId = nodeId;
	}

	public LogicNode(List<Node> stmts, List<LogicNode> subNodes, Location location, int layer, int nodeId) {
		super();
		this.stmts = stmts;
		this.subNodes = subNodes;
		this.location = location;
		this.layer = layer;
		this.nodeId = nodeId;
	}

	@Override
	public String toString() {
		return "NodeId: " + nodeId + " Layer: " + layer + " Location: " + location.toString();
	}
	
}
