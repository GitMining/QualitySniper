package iSESniper.code.entity.em;

import java.util.Optional;

import com.github.javaparser.Range;

public class EmptyMethodEntity {
	private String fileName;
	private String methodName;
	private Optional<Range> range;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Optional<Range> getRange() {
		return range;
	}

	public void setRange(Optional<Range> range) {
		this.range = range;
	}

	@Override
	public String toString() {
		return "Method:" + this.methodName + "() in Class:" + this.fileName + "(" + this.range + ") is empty!";
	}
}
