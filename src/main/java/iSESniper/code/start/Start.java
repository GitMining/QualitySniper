package iSESniper.code.start;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.start.Start.java
 *  
 *  2018年2月26日	下午5:54:51  
*/

public interface Start {
	public void startAnalyze();
}
