package iSESniper.code.start.impl;

import iSESniper.code.analyzer.loc.impl.FileLOC;
import iSESniper.code.analyzer.loc.impl.JavaFileLOC;
import iSESniper.code.analyzer.logic.impl.MethodsCCInJava;
import iSESniper.code.analyzer.logic.impl.MethodsLDInJava;
import iSESniper.code.analyzer.vab.impl.JavaFileParams;
import iSESniper.code.analyzer.vab.impl.JavaFileVabDecs;
import iSESniper.code.entity.loc.FileLinesOfCodeEntity;
import iSESniper.code.entity.loc.JavaLinesOfCodeEntity;
import iSESniper.code.entity.vab.MethodParamsInFile;
import iSESniper.code.entity.vab.MethodVabsInFile;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.scanner.impl.FolderScanner4Java;
import iSESniper.code.start.Start;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author : Magister
 * @fileName : iSESniper.code.start.impl.ProjectAnalyzer.java
 * <p>
 * 2018年2月26日 下午5:56:20
 */

public class ProjectAnalyzer implements Start {

    private String prjt;

    private String prName;

    private ArrayList<String> jFiles;

    private ArrayList<String> allFiles;

    public ProjectAnalyzer(String prjt) throws PathNotFoundException, FilesListIsEmptyException {
        this.prjt = prjt;
        this.prName = this.getPrjtName();
        FolderScanner4Java fsj = new FolderScanner4Java();
        try {
            this.allFiles = fsj.scan(prjt);
            this.jFiles = fsj.getFiles(this.allFiles);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new PathNotFoundException(prjt);
        }
    }

    public void startAnalyze() {
        /**
         * This function create analyze log under the "LogOutput" folder under the project root.
         * If your project do not have LogOutput folder we will create it.
         */

        /*
         * PART LOC Get all non-java files' LOC.
         */
        List<FileLinesOfCodeEntity> njloc = getNJLOC();
        // System.out.println(njloc);
        // System.out.println("/n======================================/n");

        /* Get all java files' LOC. */
        List<JavaLinesOfCodeEntity> jloc = getJLOC();
        // System.out.println(jloc);

        /*
         * PART Logic Get CC.
         */
        List<Map<String, Integer>> mscc = getMethodsCC();
        // for (int i = 0; i < mscc.size(); i++) {
        // System.out.println(mscc.get(i));
        // }

        /* Get LD. */
        List<Map<String, Integer>> msld = getMethodsLD();
        // for (int i = 0; i < msld.size(); i++) {
        // System.out.println(msld.get(i));
        // }

        /*
         * Vab Part. Get methods parameters.
         */
        List<MethodParamsInFile> msps = getMethodsParams();
        // for (MethodParamsInFile mp : msps) {
        // System.out.println(mp);
        // }

        /* Get methods variable */
        List<MethodVabsInFile> msvs = getMethodsVabs();
        // for(MethodVabsInFile m: msvs) {
        // System.out.println(m);
        // }

        try {
            Log.init();

            String fn_loc = this.prName + "_LOC_result";
            List<String> loc = new ArrayList<String>();
            loc.add("Lines Of Code Part \r\nNon-Java files\n");
            loc.add(njloc.toString());
            loc.add("\n======================================\n\nJava files\n");
            loc.add(jloc.toString());
            Log.createLog(fn_loc, loc);

            String fn_logic = this.prName + "_Logic_result";
            List<String> logic = new ArrayList<String>();
            logic.add("Logic Part\r\nCyclomatic Complex\n");
            for (Map<String, Integer> ms : mscc) {
                for (String m : ms.keySet()) {
                    logic.add(m + ": " + ms.get(m) + "\n");
                }
            }
            logic.add("\n======================================\n\nMethod Logic Depth\n");
            for (Map<String, Integer> ms : msld) {
                for (String m : ms.keySet()) {
                    logic.add(m + ": " + ms.get(m) + "\n");
                }
            }
            Log.createLog(fn_logic, logic);

            String fn_vabs = this.prName + "_Vabs_result";
            List<String> vabs = new ArrayList<String>();
            vabs.add("Variable Part\r\nMethods Parameters\n");
            vabs.add(msps.toString());
            vabs.add("\n======================================\n\nMethod Variables Declaration\n");
            vabs.add(msvs.toString());
            Log.createLog(fn_vabs, vabs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<FileLinesOfCodeEntity> getNJLOC() {
        List<FileLinesOfCodeEntity> filesLOC = new ArrayList<FileLinesOfCodeEntity>();
        ArrayList<String> nJFs = this.allFiles;
        nJFs.removeAll(this.jFiles);
        FileLinesOfCodeEntity floc;
        FileLOC flocer = new FileLOC();
        for (String s : nJFs) {
            floc = flocer.getLOC(s);
            filesLOC.add(floc);
        }
        return filesLOC;
    }

    public List<JavaLinesOfCodeEntity> getJLOC() {
        List<JavaLinesOfCodeEntity> jfsLOC = new ArrayList<JavaLinesOfCodeEntity>();
        ArrayList<String> jfs = this.jFiles;
        JavaLinesOfCodeEntity jloc;
        JavaFileLOC jflocer = new JavaFileLOC();
        for (String s : jfs) {
            jloc = jflocer.getLOC(s);
            jfsLOC.add(jloc);
        }
        return jfsLOC;
    }

    public List<Map<String, Integer>> getMethodsCC() {
        ArrayList<String> pths = this.jFiles;
        MethodsCCInJava mccj = new MethodsCCInJava();
        List<Map<String, Integer>> msCC = new ArrayList<Map<String, Integer>>();
        Map<String, Integer> mCC;
        for (String s : pths) {
            if (s.equals("D:\\work\\GitMining\\Repos\\samples\\BuffettANA\\BUFF\\BuffClient\\src\\main\\java" +
                    "\\gui\\functions\\StockChangeController.java"))
                System.out.println(s);
            mCC = mccj.getMethodsCC(s);
            if (mCC.isEmpty())
                continue;
            msCC.add(mCC);
        }
        return msCC;
    }

    public List<Map<String, Integer>> getMethodsLD() {
        ArrayList<String> pths = this.jFiles;
        List<Map<String, Integer>> msLD = new ArrayList<Map<String, Integer>>();
        MethodsLDInJava mldj = new MethodsLDInJava();
        Map<String, Integer> mLD;
        for (String s : pths) {
            mLD = mldj.getMethodsLD(s);
            if (mLD.isEmpty())
                continue;
            msLD.add(mLD);
        }
        return msLD;
    }

    public List<MethodParamsInFile> getMethodsParams() {
        ArrayList<String> pths = this.jFiles;
        JavaFileParams jfps = new JavaFileParams();
        MethodParamsInFile msParams;
        List<MethodParamsInFile> rtn = new ArrayList<MethodParamsInFile>();
        for (String s : pths) {
            msParams = jfps.getVabStatistic(s);
            rtn.add(msParams);
        }
        return rtn;
    }

    public List<MethodVabsInFile> getMethodsVabs() {
        ArrayList<String> pths = this.jFiles;
        JavaFileVabDecs jfvs = new JavaFileVabDecs();
        MethodVabsInFile msvs;
        List<MethodVabsInFile> rtn = new ArrayList<MethodVabsInFile>();
        for (String s : pths) {
            msvs = jfvs.getVabStatistic(s);
            if (msvs == null)
                continue;
            rtn.add(msvs);
        }
        return rtn;
    }

    public String getPrjtName() {
        String prjt = this.prjt;
        return prjt.substring(prjt.lastIndexOf('\\') + 1);
    }

    public ArrayList<String> getjFiles() {
        return jFiles;
    }

    public ArrayList<String> getAllFiles() {
        return allFiles;
    }

    public String getPrjt() {
        return prjt;
    }

    public String getPrName() {
        return prName;
    }

}
