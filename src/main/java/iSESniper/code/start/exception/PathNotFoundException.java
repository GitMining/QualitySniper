package iSESniper.code.start.exception;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.code.start.exception.PathNotFoundException.java
 *  
 *  2018年2月26日	下午5:59:56  
*/

public class PathNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PathNotFoundException(String pth) {
		super("Can not find certain path: " + pth);
	}
	
	public PathNotFoundException() {
		super("Can not find certain path.");
	}
}
