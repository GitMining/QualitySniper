package iSESniper.code.scanner;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import iSESniper.code.scanner.exception.FilesListIsEmptyException;

public interface Scanner {
    public ArrayList<String> scan(String prjtPth) throws FileNotFoundException;
    
    public ArrayList<String> getFiles(ArrayList<String> files) throws FilesListIsEmptyException;
}
