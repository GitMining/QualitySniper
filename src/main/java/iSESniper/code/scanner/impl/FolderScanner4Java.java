package iSESniper.code.scanner.impl;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import iSESniper.code.scanner.exception.FilesListIsEmptyException;

public class FolderScanner4Java extends FileScanner {

	public ArrayList<String> getFiles(ArrayList<String> files) throws FilesListIsEmptyException {
		/**
		 * Extract all java files in scan result.
		 */
		if (files.isEmpty()) {
			throw new FilesListIsEmptyException("OPPS! Target file list is empty.");
		}
		ArrayList<String> jFiles = new ArrayList<String>();
		String regEx = "\\.java$";
		Pattern ptn = Pattern.compile(regEx);
		Matcher mtch;
		for (String file : files) {
			mtch = ptn.matcher(file);
			if (mtch.find()) {
				jFiles.add(file);
//				System.out.println(file);
			}
		}
		return jFiles;
	}

}
