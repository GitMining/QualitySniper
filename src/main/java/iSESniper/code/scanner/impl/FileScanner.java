package iSESniper.code.scanner.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import iSESniper.code.scanner.Scanner;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;

public abstract class FileScanner implements Scanner {

	public ArrayList<String> scan(String prjtPth) throws FileNotFoundException {
		/**
		 * Scan certain project folder from full path and return all files list.
		 */
		ArrayList<String> files = new ArrayList<String>();
		File fileDir = new File(prjtPth);
		if (!fileDir.isDirectory()) {
//			System.out.println(fileDir.getAbsolutePath());
			files.add(fileDir.getAbsolutePath());
		} else {
			String[] subFiles = fileDir.list();
			for (int i = 0; i < subFiles.length; i++) {
				String subPth = prjtPth + "/" + subFiles[i];
				files.addAll(scan(subPth));
			}
		}
		return files;
	}

	public abstract ArrayList<String> getFiles(ArrayList<String> files) throws FilesListIsEmptyException;

}
